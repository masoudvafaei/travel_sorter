
# Travel Sorter

- Clone The project
- Composer install
- Run project : 
```sh

php -S 127.0.0.1:3000
```
 - Call API
```sh
curl --location --request POST 'http://127.0.0.1:3000/sort' \
--header 'Content-Type: application/json' \
--data-raw '{
  "tickets": [  
    {"transport": "Flight SK455", "origin": "Gerona Airport", "destiny": "Stockholm", "seat": "3A", "extra": "Baggage drop at ticket counter 344."},
    {"transport": "Airport Bus", "origin": "Barcelona", "destiny": "Gerona Airport"},
    {"transport": "Flight SK22", "origin": "Stockholm", "destiny": "New York JFK", "gate": "22B", "seat": "7B", "extra": "Baggage will we automatically transferred from your last leg."},
    {"transport": "Train 78A", "origin": "Madrid", "destiny": "Barcelona", "seat": "45B"}
  ]
}'
```