<?php
use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();
$routes->add('sort', new Routing\Route('/sort'));

return $routes;
