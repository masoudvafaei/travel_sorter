<?php

namespace Saba\Logic\Ticket;


use Saba\Exception\ErrorCode;
use Saba\Exception\LogicException;
use Saba\Logic\Ticket\TicketSorterInterface;

/**
 * Class TicketSorter
 * @package Saba\Ticket
 */
class TicketSorter implements TicketSorterInterface
{

    /**
     * @param array $tickets
     * @return array
     * @throws LogicException
     */
    public function sort(array $tickets) : array
    {
        if (count($tickets) < 2) {
            return $tickets;
        }
        $ticketsSortedByOrigin = [];
        $ticketsSortedByDestiny = [];

        foreach ($tickets as $ticket) {
            $origin = mb_strtolower($ticket->getOrigin());
            $destiny = mb_strtolower($ticket->getDestiny());

            $ticketsSortedByOrigin[$origin] = $ticket;
            $ticketsSortedByDestiny[$destiny] = $ticket;

            if (!isset($ticketsSortedByOrigin[$destiny])) {
                $ticketsSortedByOrigin[$destiny] = null;
            }

            if (!isset($ticketsSortedByDestiny[$origin])) {
                $ticketsSortedByDestiny[$origin] = null;
            }
        }

        $destinyNullCounts = $this->countNullItems($ticketsSortedByDestiny);
        $originNullCounts = $this->countNullItems($ticketsSortedByOrigin);
        if ($destinyNullCounts > 1 || $originNullCounts > 1) {
            throw new LogicException(ErrorCode::TICKET_MISSING_CONNECTIONS,[
                "destiny_null_counts" => $destinyNullCounts,
                "origin_null_counts" => $originNullCounts,
            ]);
        }


        $firstDestiny = null;
        foreach ($ticketsSortedByDestiny as $destiny => $unused) {
            if (!$ticketsSortedByDestiny[$destiny]) {
                $firstDestiny = $destiny;
                break;
            }
        }

        if (!$firstDestiny) {
            throw new LogicException(ErrorCode::TICKET_NO_FIRST_TICKET,[]);
        }

        $sortedByConnections = [];
        $nextDestiny = $firstDestiny;
        while ($ticket = $ticketsSortedByOrigin[$nextDestiny]) {
            $sortedByConnections[] = $ticket;
            $nextDestiny = mb_strtolower($ticket->getDestiny());
        };

        return $sortedByConnections;
    }


    /**
     * @param array $array
     * @return int
     */
    private function countNullItems(array $array): int
    {
        $count = 0;
        foreach ($array as $item) {
            if (!$item) {
                $count++;
            }
        }
        return $count;
    }
}
