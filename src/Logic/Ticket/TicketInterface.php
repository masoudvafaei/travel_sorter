<?php

namespace Saba\Logic\Ticket;


/**
 * Interface TicketInterface
 * @package Saba\Logic\Ticket
 */
interface TicketInterface {


    /**
     * @return string|null
     */
    public function getTransport(): ?string;

    /**
     * @return string|null
     */
    public function getOrigin(): ?string;

    /**
     * @return string|null
     */
    public function getDestiny(): ?string;

    /**
     * @return string|null
     */
    public function getSeat(): ?string;

    /**
     * @return string|null
     */
    public function getGate(): ?string;

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @param $transport
     */
    public function setTransport($transport): void ;

    /**
     * @param $origin
     */
    public function setOrigin($origin): void;

    /**
     * @param $destiny
     */
    public function setDestiny($destiny): void;

    /**
     * @param $seat
     */
    public function setSeat($seat): void;

    /**
     * @param $gate
     */
    public function setGate($gate): void;

    /**
     * @param $desc
     */
    public function setDescription($desc): void;
}
