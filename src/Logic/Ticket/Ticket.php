<?php

namespace Saba\Logic\Ticket;

use Saba\Logic\Ticket\TicketInterface;

/**
 * Class Ticket
 * @package Saba\Ticket
 */
class Ticket implements TicketInterface
{
    /**
     * @var null
     */
    protected $transport;
    /**
     * @var null
     */
    protected $origin;
    /**
     * @var null
     */
    protected $destiny;
    /**
     * @var null
     */
    protected $seat;
    /**
     * @var null
     */
    protected $gate;
    /**
     * @var null
     */
    protected $description;

    /**
     * Ticket constructor.
     * @param null $origin
     * @param null $destiny
     * @param null $transport
     * @param null $gate
     * @param null $seat
     * @param null $desc
     */
    public function __construct($origin = null, $destiny = null, $transport = null, $gate = null, $seat = null, $desc = null)
    {
        $this->origin = $origin;
        $this->destiny = $destiny;
        $this->transport = $transport;
        $this->gate = $gate;
        $this->seat = $seat;
        $this->description = $desc;
    }

    /**
     * @return string|null
     */
    public function getTransport(): ?string
    {
        return $this->transport;
    }

    /**
     * @return string|null
     */
    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    /**
     * @return string|null
     */
    public function getDestiny(): ?string
    {
        return $this->destiny;
    }

    /**
     * @return string|null
     */
    public function getSeat(): ?string
    {
        return $this->seat;
    }

    /**
     * @return string|null
     */
    public function getGate(): ?string
    {
        return $this->gate;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param $transport
     */
    public function setTransport($transport): void
    {
        $this->transport = $transport;
    }

    /**
     * @param $origin
     */
    public function setOrigin($origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @param $destiny
     */
    public function setDestiny($destiny): void
    {
        $this->destiny = $destiny;
    }


    /**
     * @param $seat
     */
    public function setSeat($seat): void
    {
        $this->seat = $seat;
    }

    /**
     * @param $gate
     */
    public function setGate($gate): void
    {
        $this->gate = $gate;
    }


    /**
     * @param $desc
     */
    public function setDescription($desc): void
    {
        $this->description = $desc;
    }
}
