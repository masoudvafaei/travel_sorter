<?php

namespace Saba\Logic\Ticket;


/**
 * Interface TicketSorterInterface
 * @package Saba\Ticket
 */
interface TicketSorterInterface
{

    /**
     * @param array $tickets
     * @return array
     */
    public function sort(array $tickets): array;

}
