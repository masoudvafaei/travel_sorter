<?php
require_once 'vendor/autoload.php';

use Saba\Exception\ErrorCode;
use Saba\Logic\Ticket\Ticket;
use Saba\Logic\Ticket\TicketSorter;
use Saba\Exception\LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$request = Request::createFromGlobals();

$tickets = [];
if ($content = $request->getContent()) {
    $ticketsArray = json_decode($content, true);
    $tickets = [];
    foreach ($ticketsArray["tickets"] as $ticket) {
        $tickets[] =  new Ticket(
            $ticket["origin"],
            $ticket["destiny"],
            $ticket["transport"],
            $ticket["gate"],
            $ticket["seat"],
            $ticket["extra"]
        );
    }
    $ticketSorter = new TicketSorter();
    try {
        $sortedTickets = $ticketSorter->sort($tickets);
    } catch (LogicException $e) {
        echo($e->getMessage());
        die();
    }
    $result = [];
    /** @var Ticket $sortedTicket */
    foreach ($sortedTickets as $sortedTicket) {
        $result[] = [
            "origin" => $sortedTicket->getOrigin(),
            "destiny" => $sortedTicket->getDestiny(),
            "transport" => $sortedTicket->getTransport(),
            "gate" => $sortedTicket->getGate(),
            "seat" => $sortedTicket->getSeat(),
            "extra" => $sortedTicket->getDescription()
        ];
    }

    $response = new JsonResponse($result,200);
//    $response = new Response(
//        'ssss',
//        Response::HTTP_OK,
//        ['content-type' => 'text/html']
//    );
    $response->send();
} else {
    $exception = new LogicException(ErrorCode::DATA_NOT_JSON,[]);
    echo $exception->getErrorMessage();
    die();
}
