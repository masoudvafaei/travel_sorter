<?php

namespace saba\Exception;


class LogicException extends \Exception implements LogicExceptionInterface
{
    private $meta;

    private $errorCode;


    public function __construct($errorCode = 0, array $meta = array(), \Exception $previous = null)
    {
        $this->setErrorCode($errorCode);
        $this->setMeta(implode($meta));
        parent::__construct(ErrorCode::$messages[$errorCode], $errorCode, $previous);
    }


    public function setMeta($meta)
    {
        $this->meta = $meta;
        return $this;
    }


    public function getMeta()
    {
        return $this->meta;
    }


    public function setErrorCode($code)
    {
        $this->errorCode = $code;
        return $this;
    }

    public function setErrorMessage($message)
    {
        $this->message = $message;
        return $this;
    }


    public function getErrorCode()
    {
        return $this->errorCode;
    }


    public function getErrorMessage()
    {
        return $this->message;
    }
}
