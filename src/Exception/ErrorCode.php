<?php
namespace saba\Exception;


/**
 * Class ErrorCode
 * @package Saba\Exception
 */
class ErrorCode
{
    /**
     *
     */
    const ERROR = "ERROR";

    /**
     *
     */
    const INTERNAL_SERVER_ERROR = 500;
    /**
     *
     */
    const VALIDATION_FAILED = 501;
    /**
     *
     */
    const INVALID_ARGUMENT = 502;
    /**
     *
     */
    const ACCESS_DENIED = 503;
    /**
     *
     */
    const DATA_NOT_JSON = 1000;
    /**
     *
     */
    const TICKET_MISSING_CONNECTIONS = 1001;
    /**
     *
     */
    const TICKET_NO_FIRST_TICKET = 1002;

    /**
     * @var string[]
     */
    public static $messages = array(
        self::INTERNAL_SERVER_ERROR => 'server-side error happened',
        self::VALIDATION_FAILED => 'validation failed',
        self::INVALID_ARGUMENT => 'invalid argument',
        self::ACCESS_DENIED => 'access denied',
        self::DATA_NOT_JSON => 'your input data type is not JSON',
        self::TICKET_MISSING_CONNECTIONS => 'There is no connections between your tickets',
        self::TICKET_NO_FIRST_TICKET => 'algorithm cannot find your first ticket.',
    );

    /**
     * @var string[]
     */
    public static $levels = array(
        self::INTERNAL_SERVER_ERROR => self::ERROR,
        self::VALIDATION_FAILED => self::ERROR,
        self::INVALID_ARGUMENT => self::ERROR,
        self::ACCESS_DENIED => self::ERROR,
        self::DATA_NOT_JSON => self::ERROR,
        self::TICKET_MISSING_CONNECTIONS => self::ERROR,
        self::TICKET_NO_FIRST_TICKET => self::ERROR,
    );
}
