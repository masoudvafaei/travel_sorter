<?php

namespace saba\Exception;


interface LogicExceptionInterface
{
    public function getMeta();
    public function getErrorCode();
    public function getMessage();
}
